import { TestBed } from '@angular/core/testing';

import { MyspaceGuard } from './myspace.guard';

describe('MyspaceGuard', () => {
  let guard: MyspaceGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MyspaceGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
