import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyspaceGuard } from 'src/app/guards/myspace.guard';
import { VerPostGuard } from 'src/app/guards/ver-post.guard';
import { Inicio1Component } from '../inicio1/inicio1.component';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { VerPostComponent } from './inicio/ver-post/ver-post.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ReportesComponent } from './reportes/reportes.component';

import { UsuariosComponent } from './usuarios/usuarios.component';
import { VerUsuarioComponent } from './usuarios/ver-usuario/ver-usuario.component';

const routes: Routes = [
  { path: '', component: DashboardComponent,
    children: 
    [
      { path: '', component: InicioComponent},
      { path: 'ver-post', component: VerPostComponent},
      { path: 'usuarios', component: UsuariosComponent},
      { path: 'space', component: ReportesComponent},
      { path: 'ver-usuario/:id', component: VerUsuarioComponent},
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
