import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { PublicI, UsuarioI } from 'src/app/interface/usuario.interface';
import { PublicacionService } from 'src/app/services/publicacion.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  @ViewChild("userHtml", { static: false }) userHtml!:any;

  verP:boolean    =false;
  crearP:boolean  =false;
  user!:UsuarioI;
  editarP:boolean = false;

  editarPublic! :PublicI;
  form!:FormGroup;
  title = 'texto-enriquecido';

  editor:Editor;
  toolbar:Toolbar = [
     ['bold', 'italic'],
     ['underline', 'strike'],
     ['code', 'blockquote'],
     ['ordered_list', 'bullet_list'],
     [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
     ['link', 'image'],
     ['text_color', 'background_color'],
     ['align_left', 'align_center', 'align_right', 'align_justify'],

  ];
  constructor(private serviceUser : UsuarioService,
              private servicePublic : PublicacionService,
              private router:Router,
              private fb:FormBuilder) {
      this.editor = new Editor();
      this.cargarUser();
      this.cargarForm();
   }

  ngOnInit(): void {
  }

  cargarForm(){
    this.form = this.fb.group({
      titulo : ['',Validators.required],
      contenido:['']
    })
  }

  Volver(){
    this.router.navigate(['/inicio/usuarios']);
    
  }


  cargarUser(){
    let guardados = localStorage.getItem('usuarioGlobal');
    this.user = JSON.parse(guardados || '{}');

  }

  
  
  //---------CrearPost
  crearNuevoP(){
    this.verP    = false;
    this.crearP  = true;
    this.editarP = false;
  }
  
  
  Guardar(){
    let date= new Date();
    console.log(date);
    
    let post:PublicI ={
      id:this.makeRandomId(),
      usuario: this.user.usuario,
      fecha:''+date,
      titulo: this.form.value.titulo,
      contenido :this.form.value.contenido,
      cant_like :0,
      cant_dislike :0,
      lista_comentarios:[]
      
    }

    console.log(this.form.value.contenido,post.usuario);
    this.serviceUser.agregarPost(post);
    this.servicePublic.obtenerDatosDeUsers();
    this.cargarUser();
    this.verPost();
    this.form.reset();
  }

  makeRandomId(length = 4) {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
   }
   return result;
}

  //-----------verPost
  verPost(){
    this.verP    = true;
    this.crearP  = false;
    this.editarP = false;

  }


  eliminarPost(id:string){
    console.log('borrar',id);
    this.serviceUser.eliminarPost(id);
    this.servicePublic.obtenerDatosDeUsers();
    this.cargarUser();
    
  }


  editarPost(publication:PublicI){
    this.editarP = true;
    this.verP    = false;
    this.crearP  = true; 

    this.form.patchValue({
      titulo: publication.titulo,
      contenido: publication.contenido,
    });
    this.editarPublic = publication;
  }

  guardarCambios(){
    this.editarPublic.titulo    = this.form.value.titulo;
    this.editarPublic.contenido = this.form.value.contenido;
    this.serviceUser.eliminarPost(this.editarPublic.id);
    this.serviceUser.agregarPost(this.editarPublic);
    this.servicePublic.obtenerDatosDeUsers();
    this.cargarUser();
    this.verPost();
    this.form.reset();
   
  }

}
