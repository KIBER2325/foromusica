import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioI } from 'src/app/interface/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {
  usuario!:UsuarioI;
  form!:UntypedFormGroup;

  constructor(private activeRoute: ActivatedRoute, private usuarioService: UsuarioService,private router:Router, private fb:UntypedFormBuilder) { 
    this.activeRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
      this.usuario = this.usuarioService.buscarUsuario(id);
      console.log(this.usuario);

      // if (Object.keys(usuario).length===0) {
      //   this.router.navigate(['/dashboard/usuarios']);
      // }


    })
  }

  ngOnInit(): void {
  }

  Volver(){
    this.router.navigate(['/dashboard/usuarios']);

  }

}
