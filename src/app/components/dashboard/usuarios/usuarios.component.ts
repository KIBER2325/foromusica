import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import {  UsuarioI } from 'src/app/interface/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit ,AfterViewInit{
 
  


  listUsuarios:UsuarioI[]=[];
  displayedColumns: string[] = ['usuario','public','acciones'];
  dataSource !: MatTableDataSource<any>;

  @ViewChild(MatPaginator)paginator!:MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private _usuarioService:UsuarioService, 
              private _snackbar:MatSnackBar, 
              private router:Router) { 
  }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort      = this.sort;
  }

  cargarUsuarios(){
    this.listUsuarios = this._usuarioService.getListUsers();
    this.dataSource   = new MatTableDataSource(this.listUsuarios);
    this.dataSource.paginator=this.paginator;
    this.dataSource.sort=this.sort;
  }

  applyFilter(event:Event){
    const filterValue=(event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    console.log(filterValue);

  }


  eliminarUsuario(usuario:string){
    const opcion = confirm('Estas seguro de eliminar el usuario');
    if (opcion) {
    this._usuarioService.eliminarListUsers(usuario)
    this.cargarUsuarios();

    this._snackbar.open('El usuario fue eliminado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
  }

  verUsuario(usuario: string){
     if (Object.keys(this._usuarioService.getUserG()).length === 0 ) {  
       
      this.router.navigate(['inicio']);
      this._snackbar.open('Debe iniciar sesion ', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
        });

     } else {
      this.router.navigate(['inicio/ver-usuario',usuario]);
      
    }
  }


  

 

}
