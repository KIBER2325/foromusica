import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { PublicI, PublicIU, UsuarioI } from 'src/app/interface/usuario.interface';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuarioGlobal!:UsuarioI;


  listaDeUsuarios: UsuarioI[] = [
    {usuario: 'KIBER',password: 'asdas',fecha: '2022-6-28T03:30',ultima_coneccion:'Sat Jul 01 2022 11:30:41 GMT-0400 (hora de Bolivia)',cant_like:1,cant_dislike:2,lista_public:[
      {id: "r2Nv", usuario: "KIBER", fecha: "Thu Jul 10 2022 14:07:31 GMT-0400 (hora de Bolivia)",titulo:"La guitarra",contenido: "<blockquote><h1 style=\"text-align:center\"><strong><span style=\"color:#d93f0b;\">La guitarra</span></strong></h1><p style=\"text-align:center\"><span style=\"color:#d93f0b;\"></span></p><h3><img src=\"https://definicion.de/wp-content/uploads/2012/01/guitarra-electrica.jpg\" alt=\"guitar| Bonka\" width=\"569px\"></h3><h3><strong> <span style=\"color:#0e8a16;\"></span> </strong></h3></blockquote>",cant_like: 1,cant_dislike: 0,lista_comentarios: []}
    ]}
  ];

  constructor(private router:Router) { 
    this.cargarDatos();
  }

  
  //------Usaurio Global--------------------------------------
  setUsuarioGlobal(usuario:String){
    let user = this.buscarUsuario(usuario);
    this.usuarioGlobal = user;
    localStorage.setItem('usuarioGlobal',JSON.stringify(this.usuarioGlobal));
  }
  
  
  getUserG():UsuarioI{
      let guardados = localStorage.getItem('usuarioGlobal');
      this.usuarioGlobal = JSON.parse(guardados || '{}');
    return this.usuarioGlobal;
    
  }
  
  dropUsuarioGlobal(){
    localStorage.removeItem("usuarioGlobal");
    this.setUsuarioGlobal('');
  }
  
  
  getUserPass(user:String,pass:any){
    return this.listaDeUsuarios.find(element => element.password == pass && element.usuario == user) || {} as UsuarioI;
  }
  //----------------------Lista  De Usuarios----------------------------
  getListUsers(): UsuarioI[] {

    //slice retorna  una copia del array
    return this.listaDeUsuarios.slice();
  }


  eliminarListUsers(id:String){
    this.listaDeUsuarios =this.listaDeUsuarios.filter(data => {
      return data.usuario!==id; 
    })
  }

  agregarUserList(usuario:UsuarioI){
    this.listaDeUsuarios.unshift(usuario);
  }

  buscarUsuario(id: String): UsuarioI{
    //o retorna un json {} vacio
    return this.listaDeUsuarios.find(element => element.usuario === id) || {} as UsuarioI;
  }


  // modificarUsuario(user: UsuarioI){
  //   this.eliminarListUsers(user.id);
  //   this.agregarUserList(user);
  // }

  listaNamesUsers(): String []{
    this.cargarDatos();
    let list:String[] = [];
    this.listaDeUsuarios.forEach(e => list.push(e.usuario));
    return list;
  }


  cargarDatos(){

    //-------------------Users-----------------------------
    if (!localStorage.getItem("listaUsers")) {

      this.cargarDatosDeLocal();

    } else{
      //Obtiene del local y carga datos
      //localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
      let guardados = localStorage.getItem('listaUsers');
      this.listaDeUsuarios = JSON.parse(guardados || '{}');
    }


    if (!localStorage.getItem("usuarioGlobal")) {
      let guardados = localStorage.getItem('usuarioGlobal');
      this.usuarioGlobal = JSON.parse(guardados || '{}');
    } 

  }

  cargarDatosDeLocal(){
    //carga el local con datos y vuelve a treaerlos
      localStorage.setItem("listaUsers", JSON.stringify( this.listaDeUsuarios));
      let guardados = localStorage.getItem('listaUsers');
      this.listaDeUsuarios = JSON.parse(guardados || '{}');
  }


  //-----------------Ingresar Un Nuevo post---------------------
  agregarPost(post:PublicI){
    let user = this.buscarUsuario(post.usuario);
    user.lista_public.push(post);
    this.eliminarListUsers(user.usuario);
    this.agregarUserList(user);
    this.cargarDatosDeLocal();
    this.setUsuarioGlobal(user.usuario);
  }


  eliminarPost(id:string){
    this.getUserG();
    console.log(this.usuarioGlobal);
    
    this.usuarioGlobal.lista_public = this.usuarioGlobal.lista_public.filter(e =>{
      return e.id!== id;
    });
    this.eliminarListUsers(this.usuarioGlobal.usuario);
    this.agregarUserList(this.usuarioGlobal);
    this.cargarDatosDeLocal();
    this.setUsuarioGlobal(this.usuarioGlobal.usuario);
  }




  //-----------------Ingresar Un comentario---------------------
  agregarPostPorComent(post:PublicI){
    let user = this.buscarUsuario(post.usuario);
    console.log(post);
    
    user.lista_public = user.lista_public.filter(data => {
      console.log(data.id);
      console.log(post.id);
      
      return data.id!==post.id; 
    })
    console.log(user.lista_public);

    user.lista_public.push(post);
    this.eliminarListUsers(user.usuario);
    this.agregarUserList(user);
    this.cargarDatosDeLocal();
    //actualizar al usuarioG ya que el comentario puede ser a si mismo
    //y si no lo es no pasa nada si lo actualizamos
    let userg = this.getUserG();
    this.setUsuarioGlobal(userg.usuario); 
  }
  
}
