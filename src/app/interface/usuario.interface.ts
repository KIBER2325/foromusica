export interface UsuarioI  {
  usuario:String;
  password:string;
  fecha: string;
  ultima_coneccion:string;
  cant_like:number;
  cant_dislike:number;
  lista_public:PublicI[];
}

  export interface PublicIU {
    usuario: String;
    fecha: string;
    contenido:string;
    cant_like:number;
    cant_dislike:number;
  }

export interface PublicI {
  id:string;
  usuario: String;
  fecha: string;
  titulo: string;
  contenido:string;
  cant_like:number;
  cant_dislike:number;
  lista_comentarios:PublicIU[];
}
